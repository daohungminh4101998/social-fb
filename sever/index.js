const express = require('express')
const app = express()
const port = 8000
var cors = require('cors')
const fs = require('fs');
const { promisify } = require('util')
const pipeline = promisify(require("stream").pipeline)
const path = require('path');
app.use(express.static('public'))
app.use(express.static('files'))
app.use(cors())

var multer = require('multer')
var userImg = [];
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public')
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + '-' + file.originalname)
  }
})
var upload = multer({ storage: storage }).single('file');

const dataUser = [
  {
    id: 1,
    name: 'minh',
    tampArr: [],
  },
  {
    id: 2,
    name: 'hong',
    tampArr: [],
  }
]


app.get('/', (req, res) => {
  // res.json({name:'daohungminh',pass:'123'})

  res.json(
    [{ id: "1", img: ["https://kenh14cdn.com/thumb_w/620/2020/10/10/a85419ec-c269-4f90-931c-ebc5df4d4ecb-1602297229979863541654.jpg", "https://kenh14cdn.com/thumb_w/620/2020/10/10/98ede4f0-241a-4903-a08a-b0f84c3ba323-16022972299621082926522.jpg", "https://i.pinimg.com/236x/20/50/8e/20508ed3ec31d4d392ab0eb14a9d08db.jpg", "https://i.pinimg.com/236x/f4/9a/27/f49a27d54af4bc9076f6b5dcf90a3172.jpg", "https://i.pinimg.com/236x/e9/0d/3b/e90d3b835495ac03f5d19051286326d9.jpg", "https://i.pinimg.com/236x/cd/36/6f/cd366fce4adf70dd0e7540499b507622.jpg", "https://i.pinimg.com/236x/b8/e4/22/b8e422b0b481274c8b98f78eff1f935d.jpg", "https://i.pinimg.com/236x/69/1b/4d/691b4d09a6d3463b6dfbb88cdecadad5.jpg", "https://i.pinimg.com/236x/62/3f/50/623f5007f63d8ed9d30544c47876486a.jpg", "https://i.pinimg.com/236x/5a/ea/c3/5aeac38ec144d497bd4ad361a61e901f.jpg", "https://i.pinimg.com/236x/a2/a7/5b/a2a75ba2bb4249e4db859ea8ee13bc04.jpg", "https://i.pinimg.com/236x/fd/3d/81/fd3d81c33df35da936b6d3a7174ead5e.jpg", "https://i.pinimg.com/236x/ad/4b/78/ad4b78857f3042c06b7ee5d64571b875.jpg", "https://i.pinimg.com/236x/31/6b/a2/316ba26991da33894cc91f0923987f0b.jpg", "https://i.pinimg.com/236x/04/04/f5/0404f53e9afd4a6235b4904bbb9e8c17.jpg", "https://i.pinimg.com/236x/cb/90/4a/cb904a74fe92bbfc2252853ab2f610cd.jpg"] }, { id: "2", img: ["https://i.pinimg.com/236x/12/c2/ef/12c2ef913a9c6d642014a9e76a87f44e.jpg", "https://i.pinimg.com/236x/61/e7/c5/61e7c56f817303df8bc16196c7cf363f.jpg", "https://i.pinimg.com/236x/65/55/14/655514d28f34fa758760c03feaa17979.jpg", "https://i.pinimg.com/236x/fb/0e/c6/fb0ec64f4cdfad51264005865c72aa77.jpg", "https://i.pinimg.com/236x/fb/e8/73/fbe8731006590cc4a94aa16784c93163.jpg", "https://i.pinimg.com/236x/4c/49/fb/4c49fb95c97191983dcfb98695142e78.jpg", "https://i.pinimg.com/236x/5c/63/69/5c6369d376a0d83e6ada8436667bcd4c.jpg", "https://i.pinimg.com/236x/26/ab/e8/26abe82c43a1328d1461c9ba9157039f.jpg", "https://i.pinimg.com/236x/77/b0/36/77b036eab4db7e3fe4857f0ee92e1f1d.jpg", "https://i.pinimg.com/236x/6d/1c/00/6d1c00c47d7f8aab2a765d3ca2c36a86.jpg", "https://i.pinimg.com/236x/48/94/44/48944441aa99349a6fcc5f73b6fe3184.jpg"] }, { id: "3", img: ["https://i.pinimg.com/236x/b4/32/e4/b432e4ff7e07d86f80653345fefc8251.jpg", "https://i.pinimg.com/236x/52/bb/c8/52bbc8debc84ee8729b5d4a036777b96.jpg", "https://znews-photo.zadn.vn/w480/Uploaded/kbd_pilk/2020_09_21/234.jpg", "https://kenh14cdn.com/thumb_w/620/2017/14-1510116369682.jpg", " https://znews-photo.zadn.vn/w480/Uploaded/kbd_pilk/2020_08_31/avahh.jpg", "https://znews-photo.zadn.vn/w480/Uploaded/ycgvppwi…327288_1501066326695946_4552280929892040704_o.jpg", "https://znews-photo.zadn.vn/w480/Uploaded/pcgmvvbb/2020_10_02/photo_1_15939714636631267209838.jpg", "https://znews-photo.zadn.vn/w480/Uploaded/pcgmvvbb…_goi_cam_tiet_lo_2_vo_chong_chua_co_bu_c90c3e.jpg", "https://s3.amazonaws.com/uifaces/faces/twitter/wegotvices/128.jpg", "https://znews-photo.zadn.vn/w480/Uploaded/pcgmvvbb…_28/photo_0003_2020_09_28_FNJUNJ3100AJ0003NOS.jpg", "https://s3.amazonaws.com/uifaces/faces/twitter/kalmerrautam/128.jpg", "https://znews-photo.zadn.vn/w660/Uploaded/kbd_pilk/2019_03_24/ngo_can_ngon.jpg", "https://znews-photo.zadn.vn/w660/Uploaded/kbd_pilk/2019_03_24/ngo_can_ngon.jpg", "https://znews-photo.zadn.vn/w660/Uploaded/kbd_pilk/2019_03_24/co_luc_na_trat2.jpg", "https://znews-photo.zadn.vn/w660/Uploaded/kbd_pilk/2019_03_24/ton_le.jpg", "https://znews-photo.zadn.vn/w660/Uploaded/kbd_pilk/2019_03_24/luu_diecphi.jpg", "https://s3.amazonaws.com/uifaces/faces/twitter/agustincruiz/128.jpg", "https://znews-photo.zadn.vn/w660/Uploaded/kbd_pilk/2019_03_24/trieu_le_dinh.jpg", "https://znews-photo.zadn.vn/w660/Uploaded/kbd_pilk/2019_03_24/duong_mich.jpg", "https://kenh14cdn.com/thumb_w/620/2020/5/11/h5-1589196576497760548656.jpg", "https://kenh14cdn.com/thumb_w/620/2020/5/11/h6-1589196603961390408686.png", "https://kenh14cdn.com/thumb_w/620/2020/5/11/h7-1589196653654386018943.jpeg", "https://kenh14cdn.com/thumb_w/620/2020/5/11/23dfaa…ad70a835r1-700-850v2hq-1589198828305263138204.jpg", "https://kenh14cdn.com/thumb_w/620/2020/5/11/h11-1589196767690466327426.jpg", "https://kenh14cdn.com/thumb_w/620/2020/5/11/h10-15891967376911208624122.png", "https://s3.amazonaws.com/uifaces/faces/twitter/tgormtx/128.jpg", "https://s3.amazonaws.com/uifaces/faces/twitter/sergeysafonov/128.jpg", "https://s3.amazonaws.com/uifaces/faces/twitter/nasirwd/128.jpg", "https://s3.amazonaws.com/uifaces/faces/twitter/dparrelli/128.jpg", "https://s3.amazonaws.com/uifaces/faces/twitter/vovkasolovev/128.jpg", "https://s3.amazonaws.com/uifaces/faces/twitter/bolzanmarco/128.jpg", "https://s3.amazonaws.com/uifaces/faces/twitter/brenmurrell/128.jpg", "https://s3.amazonaws.com/uifaces/faces/twitter/homka/128.jpg", "https://s3.amazonaws.com/uifaces/faces/twitter/joemdesign/128.jpg", "https://s3.amazonaws.com/uifaces/faces/twitter/nitinhayaran/128.jpg", "https://s3.amazonaws.com/uifaces/faces/twitter/kianoshp/128.jpg", "https://s3.amazonaws.com/uifaces/faces/twitter/kirangopal/128.jpg"] }]
  )
})

app.get('/demo', (req, res) => {
  // res.json({name:'daohungminh',pass:'123'})

  res.json({ name: 'demo', pass: 'demo' })
})


app.get('/upload', (req, res) => {
  // res.json({name:'daohungminh',pass:'123'})

  res.json(dataUser)
})

app.post('/upload', (req, res) => {
  // res.json({name:'daohungminh',pass:'123'})

  upload(req, res, function (err) {
    if (err instanceof multer.MulterError) {
      return res.status(500).json(err)
    } else if (err) {
      return res.status(500).json(err)
    }

    for (let index = 0; index < dataUser.length; index++) {


      if (dataUser[index].name == req.body.name) {
        dataUser[index].tampArr.push(req.file.filename)
        console.log('true')
        return res.status(200).send(dataUser[index].tampArr)

      }
      else {
        console.log('false')
      }
    }


  })

})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})