import React from "react"
import { Row, Col, Button, Spinner } from "reactstrap"
import Breadcrumbs from "../../../components/@vuexy/breadCrumbs/BreadCrumb"
import ProfileHeader from "./ProfileHeader"
import AboutCard from "./AboutCard"
import SuggestedPages from "./SuggestedPages"
import TwitterFeed from "./TwitterFeeds"
import Posts from "./Posts"
import LatestPhotos from "./LatestPhotos"
import Suggestions from "./Suggestions"
import Polls from "./Polls"

import "../../../assets/scss/pages/users-profile.scss"

class Profile extends React.Component {
  state = {
    isLoading: false
  }

  toggleLoading = () => {
    this.setState({
      isLoading: true
    })

    setTimeout(() => {
      this.setState({
        isLoading: false
      })
    }, 2000)
  }
  // <AboutCard />
  /*
   <Breadcrumbs
            breadCrumbTitle="Profile"
            breadCrumbParent="Pages"
            breadCrumbActive="Profile"
          />
           */
  render() {
    return (
      <div>
        profile
      </div>
    )
  }
}

export default Profile
