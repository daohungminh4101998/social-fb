import logo from './logo.svg';
import React from "react";
import './App.css';
import Profile from './views/pages/profile/Profile';
import './index.scss'
import axios from 'axios'
import { useEffect, useState } from 'react';

import {
  Row, Col, Spinner, Button,
  Navbar,
  NavbarToggler,
  Collapse,
  Nav
  , NavItem
} from "reactstrap"
import ProfileHeader from './views/pages/profile/ProfileHeader';
import SuggestedPages from './views/pages/profile/SuggestedPages';
import Posts from './views/pages/profile/Posts';
import LatestPhotos from './views/pages/profile/LatestPhotos';
import Suggestions from './views/pages/profile/Suggestions';
import Polls from './views/pages/profile/Polls';
import Login from './views/pages/profile/Login';
import coverImg from "./assets/img/profile/user-uploads/cover.jpg"
import dhm from "./assets/img/profile/user-uploads/dhm.JPG"
import { Edit2, Settings, Menu, X } from "react-feather"
import {
  BrowserRouter as Router,
  Link,
  Route,
  Switch,
  Redirect

} from 'react-router-dom';
function App() {
  const [stateUrl, setStateUrl] = useState('');
  const [loggedIn, setLoggedIn] = useState(false);
  let count = 1;
  useEffect(() => {
    localStorage.getItem('name')
    if (localStorage.getItem('name') == null) {
      setLoggedIn(false)
    } else {
      setLoggedIn(true)
    }
    const tampUrl = window.location.href;
    setStateUrl(tampUrl)
  }, [])

  const [stateFile, setStateFile] = useState(null);
  const [nameFile, setNameFile] = useState(null);
  const [tampImg, setTampImg] = useState();
  const [tampUser, setTampUser] = useState([]);
  const handleUploadFile = (e) => {
    setNameFile(e.target.files[0].name)
    setStateFile(e.target.files[0])
  }
  const onUploadFile = (e) => {

    const data = new FormData();
    data.append("file", stateFile);
    data.append("name", "hong");
    axios.post('http://192.168.1.131:8000/upload', data)
      .then((res) => {

        setTampImg(res.data)
        setTampUser(res.data.filename)
        if (res.status == 200) {
          alert('upload thanh cong', window.location.reload())
        }
      })
      .catch((err) => {
        console.log(err);

      })
  }
  const [dataUser, setDataUser] = useState()
  return (
    <div className="AppName">

      <Router>
        <React.Fragment>
          <Row>
            <Col sm="12">

              <input type="file" id="file" name="file" onChange={handleUploadFile} />
              <button onClick={onUploadFile}>Upload file</button>
            </Col>
          </Row>
          <div id="user-profile">
            <Row>
              <Col sm="12">
                <div className="profile-header mb-2">
                  <div className="position-relative">
                    <div className="cover-container">
                      <img
                        src={coverImg}
                        alt="CoverImg"
                        className="img-fluid bg-cover w-100 rounded-0"
                      />
                    </div>
                    <div className="profile-img-container d-flex align-items-center justify-content-between">
                      <img
                        src={dhm}
                        alt="porfileImg"
                        className="img-fluid img-border rounded-circle box-shadow-1"
                      />
                      <div className="float-right">
                        <Button color="primary" className="btn-icon rounded-circle mr-1">
                          <Edit2 size={17} />
                        </Button>
                        <Button color="primary" className="btn-icon rounded-circle">
                          <Settings size={17} />
                        </Button>
                      </div>
                    </div>
                  </div>
                  <div className="d-flex justify-content-end align-items-center profile-header-nav">
                    <Navbar expand="sm" className="w-100 pr-0">
                      <Nav className="justify-content-around w-75 ml-sm-auto navbar-nav">
                        <NavItem >
                          <Link tag="a" href="#" to="/timeline" className="font-small-3">
                            Timeline
                  </Link>
                        </NavItem>
                        <NavItem >
                          <Link tag="a" href="#" to="/about" className="font-small-3">
                            About
                  </Link>
                        </NavItem>
                        <NavItem >
                          <Link tag="a" href="#" to="/photo" className="font-small-3">
                            Photos
                  </Link>
                        </NavItem>
                        <NavItem >
                          <Link tag="a" href="#" className="font-small-3">
                            Friends
                  </Link>
                        </NavItem>
                        <NavItem >
                          <Link tag="a" href="#" className="font-small-3">
                            Videos
                  </Link>
                        </NavItem>
                        <NavItem >
                          <Link tag="a" href="#" className="font-small-3">
                            Events
                  </Link>
                        </NavItem>
                      </Nav>
                    </Navbar>
                  </div>
                </div>
              </Col>
            </Row>
            <div id="profile-info">
              <Row>
                <Col lg="3" md="12">
                  <SuggestedPages />

                </Col>
                <Col lg="6" md="12">


                  <Switch>
                    <Route
                      path='/'
                      exact={true}
                      render={props => <Posts {...props} home='home' />}
                    />
                    {/* <Route
                      path='/timeline'
                      render={props => <Posts {...props} timeline='timeline' />}
                    /> */}
                    <Route
                      path='/about'
                      render={props => <Posts {...props} about='about' />}
                    />
                    <Route
                      path='/photo'
                      render={props => <Posts {...props} photo='photo' />}
                    />
                  </Switch>
                </Col>
                <Col lg="3" md="12">
                  <LatestPhotos />
                  <Suggestions />
                  <Polls />
                </Col>
              </Row>
              <Row>
              </Row>
            </div>
          </div>

        </React.Fragment>


      </Router>
    </div >
  );
}

export default App;












// import React, { useState } from 'react';
// import PropTypes from 'prop-types';
// import axios from 'axios';
// App.propTypes = {

// };

// function App(props) {

  // const [stateFile, setStateFile] = useState(null);
  // const [nameFile, setNameFile] = useState(null);
  // const [tampImg, setTampImg] = useState();
  // const [tampUser, setTampUser] = useState([]);
//   const tampNew = []
  // const handleUploadFile = (e) => {
  //   setNameFile(e.target.files[0].name)
  //   setStateFile(e.target.files[0])
  // }
  // const onUploadFile = (e) => {
  //   const data = new FormData();
  //   data.append("file", stateFile);
  //   data.append("name", "hong");

  //   axios.post('http://localhost:8000/upload', data)
  //     .then((res) => {

  //       setTampImg(res.data)
  //       setTampUser(res.data.filename)

  //     })
  //     .catch((err) => {
  //       console.log(err);

  //     })
  // }
//   return (
//     <div>
//       hi react app
//       {tampImg != undefined ? tampImg.map((imgSub) => {
//         console.log(imgSub)
//         return (<img className='my-img-post' src={tampImg == undefined ? "" : `http://localhost:8000/${imgSub}`} />)
//       }) : ''}

      // <input type="file" id="file" name="file" onChange={handleUploadFile} />
      // <button onClick={onUploadFile}>Upload file</button>
//     </div>
//   );
// }

// export default App;