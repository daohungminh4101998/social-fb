import React from 'react';
import PropTypes from 'prop-types';
import '../../.././index.scss'

import '../../.././App.css'
import {
    Row, Col, Spinner, Button,
    Navbar,
    NavbarToggler,
    Collapse,
    Nav
    , NavItem
} from "reactstrap"
import {
    BrowserRouter as Router,
    Link,
    Route,
    Switch,
    Redirect

} from 'react-router-dom';
import coverImg from "../../.././assets/img/profile/user-uploads/cover.jpg"
import dhm from "../../.././assets/img/profile/user-uploads/dhm.JPG"
import { Edit2, Settings, Menu, X } from "react-feather"

function NavMenu(props) {
    return (
        <Row>
            <Col sm="12">
                <div className="profile-header mb-2">
                    <div className="position-relative">
                        <div className="cover-container">
                            <img
                                src={coverImg}
                                alt="CoverImg"
                                className="img-fluid bg-cover w-100 rounded-0"
                            />
                        </div>
                        <div className="profile-img-container d-flex align-items-center justify-content-between">
                            <img
                                src={dhm}
                                alt="porfileImg"
                                className="img-fluid img-border rounded-circle box-shadow-1"
                                width="50"
                                height="50"
                            />
                            <div className="float-right">
                                <Button color="primary" className="btn-icon rounded-circle mr-1">
                                    <Edit2 size={17} />
                                </Button>
                                <Button color="primary" className="btn-icon rounded-circle">
                                    <Settings size={17} />
                                </Button>
                            </div>
                        </div>
                    </div>
                    <div className="d-flex justify-content-end align-items-center profile-header-nav">
                        <Navbar expand="sm" className="w-100 pr-0">
                            <Nav className="justify-content-around w-75 ml-sm-auto navbar-nav">
                                <NavItem >
                                    <Link tag="a" href="#" to="/timeline" className="font-small-3">
                                        Timeline
                      </Link>
                                </NavItem>
                                <NavItem >
                                    <Link tag="a" href="#" to="/about" className="font-small-3">
                                        About
                        </Link>
                                </NavItem>
                                <NavItem >
                                    <Link tag="a" href="#" to="/photo" className="font-small-3">
                                        Photos
                        </Link>
                                </NavItem>
                                <NavItem >
                                    <Link tag="a" href="#" className="font-small-3">
                                        Friends
                        </Link>
                                </NavItem>
                                <NavItem >
                                    <Link tag="a" href="#" className="font-small-3">
                                        Videos
    </Link>
                                </NavItem>
                                <NavItem >
                                    <Link tag="a" href="#" className="font-small-3">
                                        Events
    </Link>
                                </NavItem>
                            </Nav>
                        </Navbar>
                    </div>
                </div>
            </Col>
        </Row>


    );
}

export default NavMenu;
{/* */ }