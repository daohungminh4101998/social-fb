import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router';

Login.propTypes = {

};

function Login(props) {
    const history = useHistory();
    const handleLogin = () => {
        localStorage.setItem('access', true);
        history.replace('/')
    }
    return (
        <div>
            <h1>login</h1>
            <button onClick={handleLogin}>Login</button>
        </div>
    );
}

export default Login;