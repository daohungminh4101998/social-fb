import React from "react";
import {
  Card,
  CardBody,
  UncontrolledTooltip,
  Input,
  Label,
  Button,
  Row, Col,
  CardHeader, CardTitle, Spinner
} from "reactstrap";
import { Heart, MessageSquare } from "react-feather";
import profileImg from "../../../assets/img/profile/user-uploads/user-01.jpg";
import postImg1 from "../../../assets/img/profile/post-media/2.jpg";
import postImg2 from "../../../assets/img/profile/post-media/25.jpg";
import person1 from "../../../assets/img/portrait/small/avatar-s-1.jpg";
import person2 from "../../../assets/img/portrait/small/avatar-s-2.jpg";
import person3 from "../../../assets/img/portrait/small/avatar-s-3.jpg";
import person4 from "../../../assets/img/portrait/small/avatar-s-4.jpg";
import person5 from "../../../assets/img/portrait/small/avatar-s-5.jpg";
import person6 from "../../../assets/img/portrait/small/avatar-s-6.jpg";
import person7 from "../../../assets/img/portrait/small/avatar-s-7.jpg";
import axios from "axios";
import Loader from "react-loader-spinner";
import ProfileHeader from "./ProfileHeader";

const users = [
  {
    imgCurrent: "",
    namePost: "dao hung minh",
    timePost: "12 Dec 2018 at 1:16 AM",
    contentPost: `  I love jujubes wafer pie ice cream tiramisu. Chocolate I love
  pastry pastry sesame snaps wafer. Pastry topping biscuit lollipop
  topping I love lemon drops sweet roll bonbon. Brownie donut icing.`,
    imgPost: "",
  },
];
class Posts extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      myImg: [],
      data: [],
      isLoading: false,
      dataDemo: [],
      commentData: [
        {
          imgAva: person1,
          nameComment: 'Ronaldo',
          contentComment: 'blockiness pandemy metaxylene speckle coppy',
          titleComment: 'I love jujubes wafer pie ice cream tiramisu. Chocolate I love pastry pastry sesame snaps wafer. Pastry topping biscuit lollipop topping I love lemon drops sweet roll bonbon. Brownie donut icing.'
        },
        {
          imgAva: person2,
          nameComment: 'Messi',
          contentComment: 'blockiness pandemy metaxylene speckle coppy',
          titleComment: 'I love jujubes wafer pie ice cream tiramisu. Chocolate I love pastry pastry sesame snaps wafer. Pastry topping biscuit lollipop topping I love lemon drops sweet roll bonbon. Brownie donut icing.'
        },
        {
          imgAva: person3,
          nameComment: 'NeyMar',
          contentComment: 'blockiness pandemy metaxylene speckle coppy',
          titleComment: 'I love jujubes wafer pie ice cream tiramisu. Chocolate I love pastry pastry sesame snaps wafer. Pastry topping biscuit lollipop topping I love lemon drops sweet roll bonbon. Brownie donut icing.'
        },


      ]
    };
  }
  //WARNING! To be deprecated in React v17. Use componentDidMount instead.


  componentWillMount() {

    const getData = async () => {
      const res = await axios.get("http://192.168.1.131:8000/")//https://5f11ab6fd5e6c90016ee4ad9.mockapi.io/api/imgArr;
      console.log(res.data)
      const tampData = res.data[2].img
      if (res.data != undefined) {
        this.setState({
          data: tampData,
          isLoading: true
        });
      }
      else {
        alert('2')
      }
    }





    axios
      .get("http://192.168.1.131:8000/upload")//https://5f11ab6fd5e6c90016ee4ad9.mockapi.io/api/imgArr

      .then((response) => {

        //console.log(response)
        // handle success
        this.setState({
          myImg: response.data[1]
        });


      })

      .catch(function (error) {
        if (error.response) {
          // The request was made and the server responded with a status code
          // that falls out of the range of 2xx
          //console.log(error.response.data);
          //console.log(error.response.status);
          //console.log(error.response.headers);
        } else if (error.request) {
          //
          // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
          // http.ClientRequest in node.js
          //console.log(JSON.stringify(error.request));
        } else {
          // Something happened in setting up the request that triggered an Error
          //console.log('Error', JSON.stringify(error.message));
        }
        //console.log(JSON.stringify(error.config));
      })
      .then(function () {
        // always executed
      });


    getData()
  }
  componentDidMount() {

  }
  render() {


    //console.log(window.location.href)

    const { isLoading, dataDemo, data, commentData, myImg } = this.state;
    const { timeline, photo, about, tampUrl, home } = this.props;
    //////console.log(tampUrl)
    if (photo == 'photo') {
      return (<div>

        <Card>
          <CardHeader>
            <CardTitle>My Photos</CardTitle>
          </CardHeader>
          <CardBody>
            <Col md="4" sm="6" className="d-flex user-latest-img">
              {myImg.tampArr != undefined ? myImg.tampArr.map((imgEle) => {

                return (

                  <img
                    src={`http://192.168.1.131:8000/${imgEle}`}
                    alt="upload1"
                    className="img-fluid rounded-sm mb-1"
                  />

                )
              }) : null}
            </Col>
          </CardBody>
        </Card>
      </div>)
    }
    if (about == 'about') {
      return (<div>about</div>)
    }
    const usersPLayer = this.state.data.map((user) => {
      return (
        <React.Fragment>
          <Card>
            <CardBody>
              <div className="d-flex justify-content-start align-items-center mb-1">
                <div className="avatar mr-1">
                  <img
                    src={user}
                    alt="avtar img holder"
                    height="45"
                    width="45"
                  />
                </div>
                <div className="user-page-info">
                  <p className="mb-0">{user.namePost}</p>
                  <span className="font-small-2">12 Dec 2018 at 1:16 AM</span>
                </div>
                <div className="ml-auto user-like">
                  <Heart fill="#EA5455" stroke="#EA5455" />
                </div>
              </div>
              <p>
                I love jujubes wafer pie ice cream tiramisu. Chocolate I love
                pastry pastry sesame snaps wafer. Pastry topping biscuit
                lollipop topping I love lemon drops sweet roll bonbon. Brownie
                donut icing.
                </p>
              <img
                src={user}
                alt="postImg1"
                className="img-fluid rounded-sm mb-2"
              />
              <div className="d-flex justify-content-start align-items-center mb-1">
                <div className="d-flex align-items-center">
                  <Heart
                    size={16}
                    className="mr-50"
                    onClick={() => this.onPressHeart()}
                  />
                    145
                  </div>
                <div className="ml-2">
                  <ul className="list-unstyled users-list m-0 d-flex">
                    <li className="avatar pull-up">
                      <img
                        src={person1}
                        alt="avatar"
                        height="30"
                        width="30"
                        id="avatar13"
                      />
                      <UncontrolledTooltip placement="bottom" target="avatar13">
                        Lai Lewandowski
                        </UncontrolledTooltip>
                    </li>
                    <li className="avatar pull-up">
                      <img
                        src={person2}
                        alt="avatar"
                        height="30"
                        width="30"
                        id="avatar14"
                      />
                      <UncontrolledTooltip placement="bottom" target="avatar14">
                        Elicia Rieske
                        </UncontrolledTooltip>
                    </li>
                    <li className="avatar pull-up">
                      <img
                        src={person3}
                        alt="avatar"
                        height="30"
                        width="30"
                        id="avatar15"
                      />
                      <UncontrolledTooltip placement="bottom" target="avatar15">
                        Alberto Glotzbach
                        </UncontrolledTooltip>
                    </li>
                    <li className="avatar pull-up">
                      <img
                        src={person4}
                        alt="avatar"
                        height="30"
                        width="30"
                        id="avatar16"
                      />
                      <UncontrolledTooltip placement="bottom" target="avatar16">
                        George Nordic
                        </UncontrolledTooltip>
                    </li>
                    <li className="avatar pull-up">
                      <img
                        src={person5}
                        alt="avatar"
                        height="30"
                        width="30"
                        id="avatar17"
                      />
                      <UncontrolledTooltip placement="bottom" target="avatar17">
                        Vinnie Mostowy
                        </UncontrolledTooltip>
                    </li>
                    <li className="d-flex align-items-center pl-50">
                      <span className="align-middle">+140 more</span>
                    </li>
                  </ul>
                </div>
                <p className="ml-auto">
                  <MessageSquare size={16} className="mr-50" />
                    77
                  </p>
              </div>


              {commentData.map((comment) => {
                return (
                  <div className="d-flex justify-content-start align-items-center mb-1">
                    <div className="avatar mr-50">
                      <img src={comment.imgAva} alt="Avatar" height="30" width="30" />
                    </div>
                    <div className="user-page-info">
                      <h6 className="mb-0">{comment.nameComment}</h6>
                      <span className="font-small-2">
                        {comment.contentComment}
                      </span>

                    </div>
                  </div>
                )
              })}
              <div className="ml-auto cursor-pointer">
                <Heart className="mr-50" size={15} />
                <MessageSquare className="mr-50" size={15} />
              </div>

              <fieldset className="form-label-group mb-50">
                <Input
                  type="textarea"
                  rows="3"
                  placeholder="Add Comment"
                  id="add-comment"
                />
                <Label for="add-comment">Add Comment</Label>
              </fieldset>
              <Button size="sm" color="primary">
                Post Comment
                </Button>
            </CardBody>
          </Card>
        </React.Fragment>
      );
    });
    return isLoading == true ? usersPLayer : 'loading...'
  }



}
export default Posts;
